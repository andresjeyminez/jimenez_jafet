﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using JafetJimenez;


public class NewBehaviourScript : MonoBehaviour
{
    public InputField InputMenu;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Loop();
    }

    void Loop()
    {
        IncaKola inca = new IncaKola();
        inca.precio = 2;
        CocaCola coca = new CocaCola();
        coca.precio = 2.5;
        Spritee spritee = new Spritee();
        spritee.precio = 1;
        Cielo cielo = new Cielo();
        cielo.precio = 1;
        SanLuis sanluis = new SanLuis();
        sanluis.precio = 2;
        Maracuya maracuya = new Maracuya();
        maracuya.precio = 1.5;
        Limonada limonada = new Limonada();
        limonada.precio = 1;
        Chicha chicha = new Chicha();
        chicha.precio = 2;
        VinoBlanco vinoblanco = new VinoBlanco();
        vinoblanco.precio = 10;
        VinoDulce vinodulce = new VinoDulce();
        vinodulce.precio = 15;
        VinoTinto vinotinto = new VinoTinto();
        vinotinto.precio = 20;

        Debug.Log("Menú");
        Debug.Log("1 - Gaseosa");
        Debug.Log("2 - Agua");
        Debug.Log("3 - Refresco");
        Debug.Log("4 - Vino");

        string str = InputMenu.text;


        switch (str)
        {
            case "1":
                Debug.Log("1-Inca cola");
                Debug.Log("2-Coca cola");
                Debug.Log("3-Sprite");
                string precios = "4"; // Console.ReadLine();

                switch (precios)
                {
                    case "1":
                        Debug.Log(inca.precio);
                        break;
                    case "2":
                        Debug.Log(coca.precio);
                        break;
                    case "3":
                        Debug.Log(spritee.precio);
                        break;
                }
                break;

            case "2":
                Debug.Log("1-Cielo");
                Debug.Log("2-San luis");
                string precio2 = "2"; // Console.ReadLine();

                switch (precio2)
                {
                    case "1":
                        Debug.Log(cielo.precio);
                        break;
                    case "2":
                        Debug.Log(sanluis.precio);
                        break;

                }
                break;
            case "3":
                Debug.Log("1-Maracuya");
                Debug.Log("2-Limonada");
                Debug.Log("3-Chicha");
                string precio3 = "2";//Console.ReadLine();

                switch (precio3)
                {
                    case "1":
                        Debug.Log(maracuya.precio);
                        break;
                    case "2":
                        Debug.Log(limonada.precio);
                        break;
                    case "3":
                        Debug.Log(chicha.precio);
                        break;
                }
                break;

            case "4":
                Debug.Log("1-Vino blanco");
                Debug.Log("2-Vino Tinto");
                Debug.Log("3-Vino dulce");
                string precio4 = "2";//Console.ReadLine();

                switch (precio4)
                {
                    case "1":
                        Debug.Log(vinoblanco.precio);
                        break;
                    case "2":
                        Debug.Log(vinotinto.precio);
                        break;
                    case "3":
                        Debug.Log(vinodulce.precio);
                        break;
                }
                break;

            default:
                Debug.Log("ERROR!!");
                break;

        }
    }
}
